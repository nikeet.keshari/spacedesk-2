# dbSpace : Team-15
## Frontend Code: React
All the code files are present in the frontend folder of the repository

## Backend Code: Spring
All the code files are present in the backend folder of the repository

## CI/CD triggerns and Endpoints 
Documented in CI folder

## SCRUM Documents
All the screenshots and excel are present in docs folder

# The project demo video is added at the root of this project